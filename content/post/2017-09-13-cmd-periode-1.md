#2017-09-13 CMD Periode 1

Vandaag hebben we ons paperprototype getest en een analyse gemaakt van hoe andere ons paperprototype ervaarde.

![Onderzoek 1](https://www.youtube.com/watch?v=Uka78XAqatM&t=1s)
![Onderzoek 2](https://www.youtube.com/watch?v=d5QyYmwNn00&t=1s)

Paperprototype

![Startscherm](Start Scherm.jpeg)
 
![Team indeling](Groep Maken.jpeg)
 
![Hoofdmenu](Hoofdmenu.jpeg)
 
![Vanuit het menu naar de kaart](Kaart.jpeg)
 
![Vanuit de kaart klikken op een te bouwen gebouw](Bouwbenodigdheden.jpeg)
 
![Vanuit de kaart lopen naar een materiaal plek en hierop klikken](Materiaal Ontvangst.jpeg)
 
![Vanuit de kaart naar een te bouwen gebouw lopen en deze bouwen](Bouw Animatie.jpeg)
 
![Vanuit de kaart één van twee bovenstaande doen geeft een extra informatie scherm](Gebouw Informatie.jpeg)
 
![Vanuit hoofdmenu naar knopje team (teamverdeling van je eigen team en anderen)](Groepsverdelingen.jpeg)
 
![Vanuit hoofdmenu op knopje inventaris (inventaris van je materialen)](Inventaris.jpeg)
 
![Vanuit hoofdmenu op knopje building (bouwprogressie)](Bouw Progressie.jpeg)
 
![Vanuit hoofdmenu op knopje ruilen (ruilscherm waarmee je kan ruilen met anderen)](Ruilen.jpeg)
 
![Vanuit hoofdmenu naar instellingen (hier kan je instellingen aanpassen)](Instellingen.jpeg)

Spelanalyse

![Spelanalyse](Spelanalyse CMD.pdf)
 
