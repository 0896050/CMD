#2017-09-05

Vandaag hebben we een onderzoek gehouden onder bouwkunde studenten. Uiteindelijk hebben we 8 studenten geinterviewd en iedereen van ons groepje heeft minstens 1 interview gedaan. Tijdens de les zullen we deze resultaten samen voegen.

![School](Bouwkunde school 1.jpeg)
![School](Bouwkunde school 2.jpeg)

!audio[Interview Evelien](Interview Evelien.mp4)
![Interview Evelien.](Interview Evelien.jpeg)

!audio[Interview Sverre](Interview Sverre.mp4)
![Interview Sverre](Interview Sverre.jpeg)

!audio[Interview Marleen](Interview Marleen.mp4)
![Interview Marleen](Interview Marleen.jpeg)

!audio[Interview Iris](Interview Iris.mp4)
![Interview Iris](Interview Iris.jpeg)

!audio[Interview Rick](Interview Rick.mp4)
![Interview Rick](Interview Rick.jpeg)

