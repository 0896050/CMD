#2017-09-04 CMD Periode 1

Vandaag zijn we begonnen aan het project. Ik zit in een groepje wat we "Out of the box" genoemd hebben. 
Mijn teamleden zijn:

- Rick Pijbes 0896050
- Iris Brussee 0949036
- Evelien van Driel Petri 0947743
- Marleen Boots 0934386
- Sverre lems 0944504

![Groepje](Groepje.jpeg)

![Brainstormen](Brainstormen.jpeg)
![The making of](Maken.jpeg)
![Out of the box](Out of the box.jpeg)

De bedoeling is dat je een game bedenkt met één online en één offline element voor een bepaalde doelgroep van de hogeschool om voor de eerstejaars verbondenheid met elkaar en Rotterdam te creëren en te stimuleren. 
Na enig brainstormen hebben we de doelgroep Bouwkunde studenten gekozen omdat er volgens ons veel te verzinnen is vanwege het feit dat Rotterdam naar ons idee een levende en bruisende stad is met een enorme verscheidenheid aan verschillende architectuur. Dit bied ons opties voor eventuele concepten als het gaat om de verbondenheid met Rotterdam. Onze aanname van Bouwkunde studenten is namelijk dat ze interesse zullen hebben in de verscheidenheid van stijlen die in Rotterdam te zien zijn. 
We hebben een vragenlijst opgesteld bestaande uit 8 vragen om bovenstaande aanname te testen en extra informatie te verzamelen van de gemiddelde Bouwkunde student.
(zie word document)

Als groep hebben we daarna besloten dat wij op Dinsdag 05-09-2017 langs zullen gaan bij de studie Bouwkunde om studenten te interviewen.

De rest van de dag heb ik zelf geprobeerd een begin van een moodboard te maken.
![Moodboard](Moodboard.png)
