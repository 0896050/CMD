#2017-09-11 CMD Periode 1

Vandaag hebben we elkaars concepten bekeken en één concept gekozen voor onze eerste iteratie. Het idee van Evelien. We doen hier nog wat aanpassingen aan.

![ArchiLike Rick](ArchiLike.jpeg)
![Rotterdam Finder Rick](Rotterdam Finder.jpeg)

![Concept Sverre](concept sverre.docx)

![Concept Iris](Game concept Iris.pdf)

![Concept Marleen](Game concept The Box Marleen.pdf)

![Concept Evelien 1](CONCEPT GAME 1 EVELIEN.pdf)
![Concept Evelien 2](CONCEPT GAME 2 EVELIEN.pdf)

Ook hebben we ons blog in elkaar gezet.