#2017-09-20 CMD Periode 1

Vandaag hebben we een ontwerpproces gemaakt van onze progressie van iteratie 1 tot en met het einde van iteratie 2. Hierin hebben we duidelijk aangegeven wat het gelopen pad is en wat het te belopen pad word.

![Ontwerpproces](Ontwerpproces.jpeg)

Toen was het tijd en genoodzaakt voor ons groepje om een duidelijke nieuwe planning te maken met taakverdelingen en eventuele dead-lines.

![Planning Per Dag](Planning Per Dag.jpeg)
![To Do List](To Do List.jpeg)

Ook hebben we verschillende brainstormsessies gedaan voor de 2e iteratie. Als eerste hebben we 2 mindmaps gemaakt.

![Mind Map Concept](Mindmap Concept.jpeg)
![Mind Map Spelsoorten](Mindmap Spelsoorten.jpeg)

Daarna hebben we de 6-3-1 methode gedaan met ons groepje (eigenlijk dus 5-3-1)

![6-3-1](531 (1).jpeg)
![6-3-1](531 (2).jpeg)
![6-3-1](531 (3).jpeg)
![6-3-1](531 (4).jpeg)
![6-3-1](531 (5).jpeg)

Toen hebben we geprobeerd een mindmap te maken over concept ideeën.

![Game Concepten](Game Concepten.jpeg)

Als laatste hebben we besloten welke spellen we gaan analyseren. 
Zelf ga ik het drank/kaart spel kingsen analyseren.
