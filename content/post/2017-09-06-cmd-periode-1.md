#2017-09-06 CMD Periode 1

Vandaag hebben ik alle resultaten van het onderzoek bij elkaar gevoegd 
![Resultaten onderzoek](Interview Gecombineerd.docx)

We hebben een planning gemaakt via planner van outlouk waarbij iedereen kan aanvinken wat hij heeft gedaan en wanneer men daar klaar mee is.  Op deze manier kan iedereen van het groepje op de hoogte zijn van de activiteiten van de ander.
[Planner: Out of the box](https://tasks.office.com/hrnl.onmicrosoft.com/en-GB/Home/Planner/#/plantaskboard?groupId=170470cb-769f-4c5d-abcd-59d08c10b220&planId=8CWQnMA3_k2Ka0GQhEO7tZYAGzZy)

Hiermee hebben we de afspraak gemaakt dat iedereen van ons groepje voor Vrijdag 08-09-2017 maximaal 3 Concepten bedenkt en deze op de google drive post.  Iedereen heeft dan de tijd om de resultaten zelf te evalueren en daar mee te kunnen werken.
Na het inleveren hiervan zullen we in het weekend elkaars ideeën bekijken en voor Maandag 11-09-2017 de leukste uit te kiezen zodat we Maandag meteen verder kunnen werken.

Als laatste ben ik begonnen met het bijhouden van alle data voor mijn blog.